Template.chat.helpers({
    "posts": function () {
        return (Posts.find({}).fetch());
    }
});

Template.people.helpers({
    "users": function () {
        return Users.find({}).fetch();
    }
});
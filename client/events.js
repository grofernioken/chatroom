Template.chat.onCreated(function (){
    Meteor.subscribe('posts');
});
Template.chat.events({
    "submit .message-form": function (event) {
        event.preventDefault();
        var message = event.target.message.value;
        
        Posts.insert({
            message: message,
            username: Meteor.user().username,
        });
    }
});